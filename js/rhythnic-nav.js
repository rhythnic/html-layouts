var RhythnicNav = {
    init : function(nav, options) {
        this.nav = nav; 
        this.options = options || {};
        this.overrideOptions(this.defaultOptions, this.options);
        this.nav.className += this.options.hideViewClass;

        //button that opens and closes nav
        this.toggleNav = document.getElementById(this.nav.getAttribute("data-toggleBtn"));
        
        this.bindEvents();
    },
    
    bindEvents : function() {
        var self = this;
        self.toggleNav.addEventListener("click", function(){
            self.nav.classList.toggle(self.options.hideViewClass);
        });
    },
    
    overrideOptions : function(defaultOptions, userOptions) {
        for (var key in defaultOptions) {
            if (!(key in userOptions)) userOptions[key] = defaultOptions[key];
        }
    },
    
    defaultOptions : {
        'hideViewClass' : "hidden"
    },
};